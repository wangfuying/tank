package level

// global map data
var GameMaps []*GameMap
var MaxLevel int = 0

func AddMap(gm *GameMap) {
	GameMaps = append(GameMaps, gm)
	MaxLevel++
}

// global data structure define
type GameMap struct {
	name        string
	logicWidth  int
	logicHeight int
	data        [][]int
}

func (gm *GameMap) String() string {
	return gm.name
}
