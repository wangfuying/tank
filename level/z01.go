package level

import (
	"fmt"
)

func init() {
	fmt.Println("Init map 01 ...")
	map01 := &GameMap{name: "第一关"}
	map01.data = [][]int{
		{1, 2, 3},
		{4, 5, 6}}
	AddMap(map01)
}
