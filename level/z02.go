package level

import (
	"fmt"
)

func init() {
	fmt.Println("Init map 02 ...")
	map02 := &GameMap{name: "第二关"}
	map02.data = [][]int{
		{1, 2, 3},
		{4, 5, 6}}
	AddMap(map02)
}
