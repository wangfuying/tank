package logic

type Tank struct {
	pixelX int
	pixelY int
	gridX  int
	gridY  int

	name string

	star  int
	shell bool

	life int
}

func NewTank(name string) *Tank {
	return &Tank{
		name:  name,
		star:  0,
		life:  1,
		shell: false,
	}
}
